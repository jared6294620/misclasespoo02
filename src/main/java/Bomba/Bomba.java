/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Bomba;

/**
 *
 * @author jared
 */
public class Bomba {
    // Atributos
    private int idBomba;
    private int contadorLitros;
    private int capacidad;
    private Gasolina gasolina;
    
    
    // Constructores
    
    // Vacío
    public Bomba() {
        this.idBomba = 0;
        this.contadorLitros = 0;
        this.capacidad = 0;
        this.gasolina = new Gasolina();
    }
     
   // Con parámetros
    public Bomba(int idBomba, int contadorLitros, int capacidad, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.contadorLitros = contadorLitros;
        this.capacidad = capacidad;
        this.gasolina = gasolina;
    }

    // Encapsulamiento
    public int getIdBomba() {
        return idBomba;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public int getContadorLitros() {
        return contadorLitros;
    }

    public void setContadorLitros(int contadorLitros) {
        this.contadorLitros = contadorLitros;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    // Comportamiento
    public float obtenerInventario() {
        return this.capacidad - this.contadorLitros;
    }
    
    public float venderGasolina(int cantidad){
        float totalVenta = 0.0f;
        
        if(cantidad <= this.obtenerInventario()) {
            totalVenta = cantidad * this.gasolina.getPrecio();
            this.contadorLitros = this.contadorLitros + cantidad;
        }
        
        return totalVenta;
    }
    
    public float totalVenta() {
        return this.contadorLitros * this.gasolina.getPrecio();
    }
    
}
