/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author jared
 */
public class EmpleadoEventual extends Empleado {
    private float pagoHora;
    private float hrsTrabajadas;
    
    // Constructores
    public EmpleadoEventual(float pagoHora, float hrsTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoHora = pagoHora;
        this.hrsTrabajadas = hrsTrabajadas;
    }

    public EmpleadoEventual(){
    super();
    this.hrsTrabajadas = 0.0f;
    this.pagoHora = 0.0f;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHrsTrabajadas() {
        return hrsTrabajadas;
    }

    public void setHrsTrabajadas(float hrsTrabajadas) {
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    @Override
    public float calcularPago() {
        float pago = 0.0f;
        pago = this.hrsTrabajadas * this.pagoHora;
        return pago;
    }
    
}
